// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STUBasePickUp.generated.h"

class USoundCue;
class USphereComponent;

UCLASS()
class SHOOTTHEMUP_API ASTUBasePickUp : public AActor
{
    GENERATED_BODY()

public:
    ASTUBasePickUp();

protected:
    virtual void BeginPlay() override;

    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

    UPROPERTY(VisibleAnywhere, Category = "PickUp")
    USphereComponent* CollisionComponent;

    UPROPERTY(VisibleAnywhere, Category = "PickUp")
    float RespawnTime = 5.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PickUp")
    USoundCue* PickupTakenSound;

public:
    virtual void Tick(float DeltaTime) override;
    bool CouldBeTaken() const;

private:
    float RotationYaw = 0.f;
    FTimerHandle RespawnTimerHandle;

    virtual bool GivePickupTo(APawn* PlayerPawn) { return false; };
    void PickUpWasTaken();
    void Respawn();
    void GenerateRotationYaw();
};
