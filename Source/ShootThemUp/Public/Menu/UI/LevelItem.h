// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "STUCoreTypes.h"
#include "UObject/NoExportTypes.h"
#include "LevelItem.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class SHOOTTHEMUP_API ULevelItem : public UObject
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintReadOnly)
    FLevelData LevelData;
    UPROPERTY(BlueprintReadWrite)
    bool bIsSelected = false;
};
