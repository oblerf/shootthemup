// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "STUGotoMenuWidget.generated.h"

class UButton;

/**
 *
 */
UCLASS()
class SHOOTTHEMUP_API USTUGotoMenuWidget : public UUserWidget
{
    GENERATED_BODY()
protected:
    UPROPERTY(meta = (BindWidget))
    UButton* GotoMainMenuButton;
    virtual void NativeOnInitialized() override;

private:
    UFUNCTION()
    void OnGotoMainMenu();
};
