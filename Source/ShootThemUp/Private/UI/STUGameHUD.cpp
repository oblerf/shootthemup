// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/STUGameHUD.h"

#include "Engine/Canvas.h"
#include "STUGameModeBase.h"
#include "UI/STUBaseWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUGameHUD, All, All);
void ASTUGameHUD::DrawHUD()
{
    Super::DrawHUD();

    // DrawCrossHair();
}

void ASTUGameHUD::BeginPlay()
{
    Super::BeginPlay();

    GameWidgets.Add(ESTUMatchState::InProgress, CreateWidget<USTUBaseWidget>(GetWorld(), PlayerHUDWidgetClass));
    GameWidgets.Add(ESTUMatchState::Pause, CreateWidget<USTUBaseWidget>(GetWorld(), PauseWidgetClass));
    GameWidgets.Add(ESTUMatchState::GameOver, CreateWidget<USTUBaseWidget>(GetWorld(), GameOverWidgetClass));

    for (auto GameWidgetPair : GameWidgets)
    {
        const auto GameWidget = GameWidgetPair.Value;
        if (!GameWidget) continue;

        GameWidget->AddToViewport();
        GameWidget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (GetWorld())
    {
        const auto GameMode = Cast<ASTUGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &ASTUGameHUD::OnMatchStateChanged);
        }
    }
}

void ASTUGameHUD::DrawCrossHair()
{

    const FVector2D Center(Canvas->SizeX * .5f, Canvas->SizeY * .5f);

    const float HalfLineSize = 10.f;
    const float LineThickness = 2.f;
    const FLinearColor LinearColor = FLinearColor::Green;

    DrawLine(Center.X - HalfLineSize, Center.Y, Center.X + HalfLineSize, Center.Y, LinearColor, LineThickness);
    DrawLine(Center.X, Center.Y - HalfLineSize, Center.X, Center.Y + HalfLineSize, LinearColor, LineThickness);
}

void ASTUGameHUD::OnMatchStateChanged(ESTUMatchState NewMatchState)
{
    if (CurrentWidget)
    {
        CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
    }
    if (GameWidgets.Contains(NewMatchState) && GameWidgets[NewMatchState])
    {
        CurrentWidget = GameWidgets[NewMatchState];
        CurrentWidget->SetVisibility(ESlateVisibility::Visible);
        CurrentWidget->Show();
    }

    UE_LOG(LogSTUGameHUD, Display, TEXT("Match state changed: %s"), *UEnum::GetValueAsString(NewMatchState));
}
