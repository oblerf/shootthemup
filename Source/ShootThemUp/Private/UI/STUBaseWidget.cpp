// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/STUBaseWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

void USTUBaseWidget::Show()
{
    PlayAnimation(OpenAnimation);
    UGameplayStatics::PlaySound2D(GetWorld(), OpenSound);
}
