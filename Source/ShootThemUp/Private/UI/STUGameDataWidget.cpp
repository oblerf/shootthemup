// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/STUGameDataWidget.h"

#include "STUGameModeBase.h"
#include "STUPlayerState.h"


int32 USTUGameDataWidget::GetCurrentRound() const
{
    const auto GameMode = GetSTUGameMode();
    return GameMode ? GameMode->GetCurrentRound() : 0;
}

int32 USTUGameDataWidget::GetTotalRound() const
{
    const auto GameMode = GetSTUGameMode();
    return GameMode ? GameMode->GetGameData().RoundsNum : 0;
}

int32 USTUGameDataWidget::GetRoundCountDown() const
{
    const auto GameMode = GetSTUGameMode();
    return GameMode ? GameMode->GetRoundCountDown() : 0;
}

ASTUGameModeBase* USTUGameDataWidget::GetSTUGameMode() const
{
    return GetWorld() ? Cast<ASTUGameModeBase>(GetWorld()->GetAuthGameMode()) : nullptr;
}

ASTUPlayerState* USTUGameDataWidget::GetSTUPlayerState() const
{
    return GetOwningPlayer() ? Cast<ASTUPlayerState>(GetOwningPlayer()->PlayerState) : nullptr;
}
