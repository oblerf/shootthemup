// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/STUGotoMenuWidget.h"
#include "STUGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUGotoMenuWidget, All, All);

void USTUGotoMenuWidget::NativeOnInitialized()
{
    if (GotoMainMenuButton)
    {
        GotoMainMenuButton->OnClicked.AddDynamic(this, &USTUGotoMenuWidget::OnGotoMainMenu);
    }
}

void USTUGotoMenuWidget::OnGotoMainMenu()
{
    if (!GetWorld()) return;

    const auto STUGameInstance = GetWorld()->GetGameInstance<USTUGameInstance>();
    if (!STUGameInstance) return;

    if (STUGameInstance->GetMainMenuLevelName().IsNone())
    {
        UE_LOG(LogSTUGotoMenuWidget, Error, TEXT("Level name is None"));
    }

    UGameplayStatics::OpenLevel(this, STUGameInstance->GetMainMenuLevelName());
}
