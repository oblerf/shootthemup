// Fill out your copyright notice in the Description page of Project Settings.

#include "AI/Service/STUChangeWeaponService.h"
#include "STUUtils.h"
#include "AIController.h"
#include "Components/STUWeaponComponent.h"
USTUChangeWeaponService::USTUChangeWeaponService()
{
    NodeName = "Change Weapon";
}

void USTUChangeWeaponService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

    const auto Controller = OwnerComp.GetAIOwner();
    if (!Controller) return;

    const auto WeaponComponent = STUUtils::GetSTUPlayerComponent<USTUWeaponComponent>(Controller->GetPawn());
    if (WeaponComponent && Probability > 0 && FMath::FRand() <= Probability)
    {
        WeaponComponent->NextWeapon();
    }
}
