// Fill out your copyright notice in the Description page of Project Settings.

#include "Menu/UI/STUMenuWidget.h"

#include "LevelItem.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"
#include "STUGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/HorizontalBox.h"
#include "STULevelItemWidget.h"
#include "Components/ListView.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUMenuWidget, All, All);

void USTUMenuWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    InitLevelItems();
    if (StartGameButton)
    {
        StartGameButton->OnClicked.AddDynamic(this, &USTUMenuWidget::OnStartGame);
    }
    if (QuitGameButton)
    {
        QuitGameButton->OnClicked.AddDynamic(this, &USTUMenuWidget::OnQuitGame);
    }
}

void USTUMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);

    if (Animation != HideAnimation)return;
    const auto STUGameInstance = GetSTUGameInstance();
    if (!STUGameInstance) return;

    UGameplayStatics::OpenLevel(this, STUGameInstance->GetStartupLevelData().LevelName);
}

void USTUMenuWidget::OnStartGame()
{
    PlayAnimation(HideAnimation);
    UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
}

void USTUMenuWidget::OnQuitGame()
{
    UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}

void USTUMenuWidget::InitLevelItems()
{
    const auto GameInstance = GetSTUGameInstance();
    if (!GameInstance) return;

    checkf(GameInstance->GetLevelsData().Num() != 0, TEXT("Levels data must not be empty!"));

    if (!LevelListView)return;
    LevelListView->ClearListItems();

    // if (!LevelItemBox) return;
    // LevelItemBox->ClearChildren();

    for (auto LevelData : GameInstance->GetLevelsData())
    {
        const auto Item = NewObject<ULevelItem>();
        Item->LevelData = LevelData;
        Item->bIsSelected = false;
        LevelListView->AddItem(Item);

        // const auto LevelItemWidget = CreateWidget<USTULevelItemWidget>(GetWorld(), LevelItemWidgetClass);
        // if (!LevelItemWidget) continue;
        //
        // LevelItemWidget->SetLevelData(LevelData);
        // LevelItemWidget->OnLevelSelected.AddUObject(this, &USTUMenuWidget::OnLevelSelected);
        //
        // LevelItemBox->AddChild(LevelItemWidget);
        // LevelItemWidgets.Add(LevelItemWidget);
    }

    if (GameInstance->GetStartupLevelData().LevelName.IsNone())
    {
        OnLevelSelected(GameInstance->GetLevelsData()[0]);
    }
    else
    {
        OnLevelSelected(GameInstance->GetStartupLevelData());
    }
}

void USTUMenuWidget::OnLevelSelected(const FLevelData& Data)
{
    const auto GameInstance = GetSTUGameInstance();
    if (!GameInstance) return;
    GameInstance->SetStartupLevelData(Data);

    // for (auto LevelItemWidget : LevelItemWidgets)
    // {
    //     if (LevelItemWidget)
    //     {
    //         const auto IsSelected = Data.LevelName == LevelItemWidget->GetLevelData().LevelName;
    //         LevelItemWidget->SetSelected(IsSelected);
    //     }
    // }
}

USTUGameInstance* USTUMenuWidget::GetSTUGameInstance() const
{
    if (!GetWorld()) return nullptr;

    return GetWorld()->GetGameInstance<USTUGameInstance>();
}
