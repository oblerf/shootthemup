// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/STUAIWeaponComponent.h"

void USTUAIWeaponComponent::StartFire()
{
    if (!CanFire()) return;

    if (CurrentWeapon->IsAmmoEmpty())
    {
        NextWeapon();
    }
    else
    {
        Super::StartFire();
    }
}

void USTUAIWeaponComponent::NextWeapon()
{
    if (!CanEquip()) return;

    int32 NextIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
    while (NextIndex != CurrentWeaponIndex && Weapons[NextIndex]->IsAmmoEmpty())
    {
        NextIndex = (NextIndex + 1) % Weapons.Num();
    }
    if (NextIndex != CurrentWeaponIndex)
    {
        CurrentWeaponIndex = NextIndex;
        EquipWeapon(CurrentWeaponIndex);
    }
}
