// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon/Components/STUWeaponFXComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

USTUWeaponFXComponent::USTUWeaponFXComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;

}

void USTUWeaponFXComponent::PlayImpactFX(const FHitResult& Hit)
{
    auto ImpactData = DefaultImpactData;
    if (Hit.PhysMaterial.IsValid())
    {
        const auto PhysMat = Hit.PhysMaterial.Get();
        if (ImpactDataMap.Contains(PhysMat)) ImpactData = ImpactDataMap[PhysMat];
    }

    UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), //
        ImpactData.NiagaraSystem,                              //
        Hit.Location,                                          //
        Hit.ImpactNormal.Rotation());                          // niagara

    auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), //
        ImpactData.DecalDate.Material,                                       //
        ImpactData.DecalDate.Size,                                           //
        Hit.Location,                                                        //
        Hit.ImpactNormal.Rotation(),                                         //
        ImpactData.DecalDate.LifeTime);                                      // decal
    if (DecalComponent)
        DecalComponent->SetFadeOut(ImpactData.DecalDate.LifeTime, ImpactData.DecalDate.FadeOutTime);

    UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.SoundCue, Hit.ImpactPoint);
}

// Called when the game starts
void USTUWeaponFXComponent::BeginPlay()
{
    Super::BeginPlay();

    // ...
}
