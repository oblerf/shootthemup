// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/STUAnimNotify.h"


void USTUAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    Super::Notify(MeshComp, Animation);
    OnNotified.Broadcast(MeshComp);
}
