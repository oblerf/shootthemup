// Fill out your copyright notice in the Description page of Project Settings.

#include "PickUps/STUAmmoPickUp.h"
#include "Components/STUWeaponComponent.h"
#include "Components/STUHealthActorComponent.h"
#include "STUUtils.h"
DEFINE_LOG_CATEGORY_STATIC(LogAmmoPickUp, All, All);

bool ASTUAmmoPickUp::GivePickupTo(APawn* PlayerPawn)
{
    const auto HealthComponent = STUUtils::GetSTUPlayerComponent<USTUHealthActorComponent>(PlayerPawn);
    if (!HealthComponent || HealthComponent->IsDead()) return false;

    const auto WeaponComponent = STUUtils::GetSTUPlayerComponent<USTUWeaponComponent>(PlayerPawn);
    if (!WeaponComponent) return false;

    return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);
}
